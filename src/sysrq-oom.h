/*
 * Copyright (c) 2019 Bastien Nocera <hadess@hadess.net>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3 as published by
 * the Free Software Foundation.
 *
 */

#include <glib.h>

gboolean sysrq_enable_oom (GError **error);
gboolean sysrq_trigger_oom (GError **error);
