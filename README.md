# Low Memory Monitor

The Low Memory Monitor is an early boot daemon that will monitor memory
pressure information coming from the kernel, and, first, send signals
to user-space applications when memory is running low, and activate
the kernel's [OOM](https://en.wikipedia.org/wiki/Out_of_memory) killer when
memory needs to be freed.

## Application/Service Developers

Developers of applications and of services are encouraged to listen to the signals
sent by the `low-memory-monitor`, either directly, or using the
[GMemoryMonitor API (WIP)](https://gitlab.gnome.org/GNOME/glib/merge_requests/1005)
in glib.

Documentation for the [D-Bus API is available here](https://hadess.pages.freedesktop.org/low-memory-monitor/).

If your system or user services/applications are handled by systemd, you'll want
to make sure that the [OOMPolicy](https://www.freedesktop.org/software/systemd/man/systemd.service.html#OOMPolicy=)
and the [OOMScoreAdjust](https://www.freedesktop.org/software/systemd/man/systemd.exec.html#OOMScoreAdjust=)
options are correctly set for your service.

## Kernel Out-Of-Memory Killer

It is enabled by default, unless the distributor changed that default at compile-time by passing
`-Dtrigger_kernel_oom=false` to meson. The user/admin can also modify
`/etc/low-memory-monitor.conf`'s contents to enable or disable it. For example:
```ini
[Configuration]
TriggerKernelOom=false
```

Or:
```ini
[Configuration]
TriggerKernelOom=true
```

## Background and other uses

### Kernel API
- [Tracking pressure-stall information (LWN)](https://lwn.net/Articles/759781/) ([patch](https://lwn.net/Articles/783520/))
- [Upstream documentation](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/Documentation/accounting/psi.rst)

### Other implementations
- [system-wide psi monitor and OOM helper](https://github.com/endlessm/eos-boot-helper/tree/master/psi-monitor)
- [Android's lowmemorykiller daemon](https://source.android.com/devices/tech/perf/lmkd) ([source](https://android.googlesource.com/platform/system/core/+/master/lmkd))
- [Facebook's oomd](https://github.com/facebookincubator/oomd) ([PDF for oomd2](https://linuxplumbersconf.org/event/4/contributions/292/attachments/285/477/LPC_oomd2_and_beyond__a_year_of_improvements.pdf))
- [WebKit's memory pressure monitor](https://github.com/WebKit/webkit/blob/master/Source/WebKit/UIProcess/linux/MemoryPressureMonitor.cpp)

## Dependencies

Requires Linux 5.2 or newer and GLib.
